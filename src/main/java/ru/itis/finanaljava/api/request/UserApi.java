package ru.itis.finanaljava.api.request;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.finanaljava.api.response.UserCreationResponse;
import ru.itis.finanaljava.dto.UserCreationRequest;

@RequestMapping("/v1/user")
public interface UserApi {

    @PostMapping
    UserCreationResponse createUser(@RequestBody UserCreationRequest creationRequest);
}
