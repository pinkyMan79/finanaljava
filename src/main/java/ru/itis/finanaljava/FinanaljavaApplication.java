package ru.itis.finanaljava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinanaljavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinanaljavaApplication.class, args);
    }

}
