package ru.itis.finanaljava.model;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "transactional")
public class TransactionalEntity extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "transaction_id")
    private UUID uuid;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "category")
    private String category;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "user_id")
    private UUID userId;
}
